/**
 * 
 */
package lambday.math.matters.demo;

/**
 * @author lambday
 *
 */
public class SimplePOJO {
	
	// Group 1 attributes
	private String attribute1Group1;
	private String attribute2Group1;
	private String attribute3Group1;
	private String attribute4Group1;
	private String attribute5Group1;
	
	// Group 2 attributes
	private String attribute1Group2;
	private String attribute2Group2;
	private String attribute3Group2;
	private String attribute4Group2;
	private String attribute5Group2;
	
	/**
	 * @return
	 */
	public boolean isGroup1() {
		return	(
			!Util.isNull(getAttribute1Group1()) ||
			!Util.isNull(getAttribute2Group1()) ||
			!Util.isNull(getAttribute3Group1()) ||
			!Util.isNull(getAttribute4Group1()) ||
			!Util.isNull(getAttribute5Group1())
		) && (
			Util.isNull(getAttribute1Group2()) &&
			Util.isNull(getAttribute2Group2()) &&
			Util.isNull(getAttribute3Group2()) &&
			Util.isNull(getAttribute4Group2()) &&
			Util.isNull(getAttribute5Group2())
		);
	}
	
	/**
	 * @return
	 */
	public boolean isGroup2() {
		return	(
			!Util.isNull(getAttribute1Group2()) ||
			!Util.isNull(getAttribute2Group2()) ||
			!Util.isNull(getAttribute3Group2()) ||
			!Util.isNull(getAttribute4Group2()) ||
			!Util.isNull(getAttribute5Group2())
		) && (
			Util.isNull(getAttribute1Group1()) &&
			Util.isNull(getAttribute2Group1()) &&
			Util.isNull(getAttribute3Group1()) &&
			Util.isNull(getAttribute4Group1()) &&
			Util.isNull(getAttribute5Group1())
		);
	}
	
	/**
	 * @return
	 */
	public boolean isInvalid() {
		return !isGroup1() && !isGroup2();
	}
	
	/**
	 * @return the attribute1Group1
	 */
	public String getAttribute1Group1() {
		return attribute1Group1;
	}
	/**
	 * @param attribute1Group1 the attribute1Group1 to set
	 */
	public void setAttribute1Group1(String attribute1Group1) {
		this.attribute1Group1 = attribute1Group1;
	}
	/**
	 * @return the attribute2Group1
	 */
	public String getAttribute2Group1() {
		return attribute2Group1;
	}
	/**
	 * @param attribute2Group1 the attribute2Group1 to set
	 */
	public void setAttribute2Group1(String attribute2Group1) {
		this.attribute2Group1 = attribute2Group1;
	}
	/**
	 * @return the attribute3Group1
	 */
	public String getAttribute3Group1() {
		return attribute3Group1;
	}
	/**
	 * @param attribute3Group1 the attribute3Group1 to set
	 */
	public void setAttribute3Group1(String attribute3Group1) {
		this.attribute3Group1 = attribute3Group1;
	}
	/**
	 * @return the attribute4Group1
	 */
	public String getAttribute4Group1() {
		return attribute4Group1;
	}
	/**
	 * @param attribute4Group1 the attribute4Group1 to set
	 */
	public void setAttribute4Group1(String attribute4Group1) {
		this.attribute4Group1 = attribute4Group1;
	}
	/**
	 * @return the attribute5Group1
	 */
	public String getAttribute5Group1() {
		return attribute5Group1;
	}
	/**
	 * @param attribute5Group1 the attribute5Group1 to set
	 */
	public void setAttribute5Group1(String attribute5Group1) {
		this.attribute5Group1 = attribute5Group1;
	}
	/**
	 * @return the attribute1Group2
	 */
	public String getAttribute1Group2() {
		return attribute1Group2;
	}
	/**
	 * @param attribute1Group2 the attribute1Group2 to set
	 */
	public void setAttribute1Group2(String attribute1Group2) {
		this.attribute1Group2 = attribute1Group2;
	}
	/**
	 * @return the attribute2Group2
	 */
	public String getAttribute2Group2() {
		return attribute2Group2;
	}
	/**
	 * @param attribute2Group2 the attribute2Group2 to set
	 */
	public void setAttribute2Group2(String attribute2Group2) {
		this.attribute2Group2 = attribute2Group2;
	}
	/**
	 * @return the attribute3Group2
	 */
	public String getAttribute3Group2() {
		return attribute3Group2;
	}
	/**
	 * @param attribute3Group2 the attribute3Group2 to set
	 */
	public void setAttribute3Group2(String attribute3Group2) {
		this.attribute3Group2 = attribute3Group2;
	}
	/**
	 * @return the attribute4Group2
	 */
	public String getAttribute4Group2() {
		return attribute4Group2;
	}
	/**
	 * @param attribute4Group2 the attribute4Group2 to set
	 */
	public void setAttribute4Group2(String attribute4Group2) {
		this.attribute4Group2 = attribute4Group2;
	}
	/**
	 * @return the attribute5Group2
	 */
	public String getAttribute5Group2() {
		return attribute5Group2;
	}
	/**
	 * @param attribute5Group2 the attribute5Group2 to set
	 */
	public void setAttribute5Group2(String attribute5Group2) {
		this.attribute5Group2 = attribute5Group2;
	}	
}
