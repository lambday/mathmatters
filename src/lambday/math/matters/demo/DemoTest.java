/**
 * 
 */
package lambday.math.matters.demo;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Random;

/**
 * @author lambday
 *
 */
public class DemoTest {	
	
	private enum Group {
		GROUP1,
		GROUP2,
		INVALID
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {		
		try {			
			long numTrials = 100;					
			if (args.length > 0) {				
				numTrials = extractNumTrials(args[0]);
//				System.out.println("Using " + numTrials + " trails.");
			}
			performTest(numTrials);
		} catch (NoSuchMethodException 
				| SecurityException 
				| IllegalAccessException 
				| IllegalArgumentException
				| InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param payloadIdentifier
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 */
	private static void performTest(long numTrials) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {			
		System.out.println("Group, Attr, IA, IB");
		String formatStr = "%s, %d, %.15f, %.15f\n";
		
		for (long i = 0; i < numTrials; ++i) {
			Group group = chooseGroupUniformlyRandomly();
			int attribute = chooseAttributeUniformlyRandomly(group);
			SkewedPOJO skewedPOJO = createSkewedPOJOForGroup(group, attribute);				
			
			double timeTakenByIdentifierA = detectGroup(new InverseSmartIdentifier(), group, skewedPOJO);
			double timeTakenByIdentifierB = detectGroup(new BruteForceIdentifier(), group, skewedPOJO);
			
			System.out.format(formatStr, group.name(), attribute, timeTakenByIdentifierA, timeTakenByIdentifierB);
		}			
	}

	/**
	 * @param s
	 * @return
	 */
	private static long extractNumTrials(String s) {
		long numSamples = 0;
		String errorMsg = "Please provide a positive long integer as program argument!";

		try {
			numSamples = Long.parseUnsignedLong(s);
		} catch (Exception e) {
			System.err.println(errorMsg);
			throw e;
		}

		return numSamples;
	}

	/**
	 * @param payloadIdentifier
	 * @param actualGroup
	 * @param skewedPOJO
	 */
	private static double detectGroup(PayloadIdentifier payloadIdentifier, Group actualGroup, SkewedPOJO skewedPOJO) {
		Group detectedGroup = Group.INVALID;		
		long startTime = System.currentTimeMillis();

		for (long i = 0; i < 1000000; ++i) {
			if (payloadIdentifier.isGroup1(skewedPOJO)) {
				detectedGroup = Group.GROUP1;
			}
			if (payloadIdentifier.isGroup2(skewedPOJO)) {
				detectedGroup = Group.GROUP2;
			}
			if (payloadIdentifier.isInvalid(skewedPOJO)) {
				detectedGroup = Group.INVALID;
			}
		}
		
		long stopTime = System.currentTimeMillis();		
		assert(actualGroup.equals(detectedGroup));
		long elapsedTime = stopTime - startTime;
		return elapsedTime/1000.0;	
	}

	private static SkewedPOJO createSkewedPOJOForGroup(Group group, int attribute) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		SkewedPOJO skewedPOJO = new SkewedPOJO();		
		
		if (group.equals(Group.GROUP1)) {
			String setterName = "setAttribute" + attribute + "Group1";
			Method setter = skewedPOJO.getClass().getMethod(setterName, String.class);			
			setter.invoke(skewedPOJO, "dummyString");
		} else {
			String setterName = "setAttribute" + attribute + "Group2";
			Method setter = skewedPOJO.getClass().getMethod(setterName, String.class);			
			setter.invoke(skewedPOJO, "dummyString");
		}
		
		return skewedPOJO;
	}

	private static int chooseAttributeUniformlyRandomly(Group group) {
		Random random = new Random();
		int bound = 0;
		
		if (group.equals(Group.GROUP1)) {
			// random numbers generated are [0, 5) := [0, 4]
			bound = 5;
		} else {
			// random numbers generated are [0, 95) := [0, 94]
			bound = 95;
		}		
		
		int randomNumber = random.nextInt(bound) + 1;		
		return randomNumber;
	}

	private static Group chooseGroupUniformlyRandomly() {
		Random random = new Random();
		
 		if (random.nextBoolean()) {
 			return Group.GROUP1;
 		} else { 			
 			return Group.GROUP2;
 		}
	}
}
