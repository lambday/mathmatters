/**
 * 
 */
package lambday.math.matters.demo;

/**
 * @author lambday
 *
 */
public interface PayloadIdentifier {
	/**
	 * @param skewedPOJO
	 * @return
	 */
	public boolean isGroup1(SkewedPOJO skewedPOJO);
	
	/**
	 * @param skewedPOJO
	 * @return
	 */
	public boolean isGroup2(SkewedPOJO skewedPOJO);
	
	/**
	 * @param skewedPOJO
	 * @return
	 */
	public boolean isInvalid(SkewedPOJO skewedPOJO);
}
