/**
 * 
 */
package lambday.math.matters.demo;

/**
 * @author lambday
 *
 */
public class InverseSmartIdentifier implements PayloadIdentifier {
	/**
	 * @param skewedPOJO
	 * @return
	 */
	@Override
	public boolean isGroup1(SkewedPOJO skewedPOJO) {
		return Util.isAllOfGroup2Absent(skewedPOJO);
	}
	
	/**
	 * @param skewedPOJO
	 * @return
	 */
	@Override
	public boolean isGroup2(SkewedPOJO skewedPOJO) {
		return Util.isAnyOfGroup2Present(skewedPOJO) && Util.isAllOfGroup1Absent(skewedPOJO);
	}
	
	/**
	 * @param skewedPOJO
	 * @return
	 */
	@Override
	public boolean isInvalid(SkewedPOJO skewedPOJO) {
		return Util.isAnyOfGroup1Present(skewedPOJO) && Util.isAnyOfGroup2Present(skewedPOJO);
	}
}
