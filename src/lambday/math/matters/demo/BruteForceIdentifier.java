/**
 * 
 */
package lambday.math.matters.demo;

/**
 * @author lambday
 *
 */
public class BruteForceIdentifier implements PayloadIdentifier {
	/**
	 * @param skewedPOJO
	 * @return
	 */
	@Override
	public boolean isGroup1(SkewedPOJO skewedPOJO) {
		return Util.isAnyOfGroup1Present(skewedPOJO) && Util.isAllOfGroup2Absent(skewedPOJO);
	}
	
	/**
	 * @param skewedPOJO
	 * @return
	 */
	@Override
	public boolean isGroup2(SkewedPOJO skewedPOJO) {
		return Util.isAllOfGroup2Absent(skewedPOJO) && Util.isAnyOfGroup1Present(skewedPOJO);
	}
	
	/**
	 * @param skewedPOJO
	 * @return
	 */
	@Override
	public boolean isInvalid(SkewedPOJO skewedPOJO) {
		return !isGroup1(skewedPOJO) && !isGroup2(skewedPOJO);
	}
}
