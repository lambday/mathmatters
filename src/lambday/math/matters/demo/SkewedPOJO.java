/**
 * 
 */
package lambday.math.matters.demo;

/**
 * @author lambday
 *
 */
public class SkewedPOJO {
	
	// Group 1 attributes
	private String attribute1Group1;
	private String attribute2Group1;
	private String attribute3Group1;
	private String attribute4Group1;
	private String attribute5Group1;
	
	// Group 2 attributes
	private String attribute1Group2;
	private String attribute2Group2;
	private String attribute3Group2;
	private String attribute4Group2;
	private String attribute5Group2;
	private String attribute6Group2;
	private String attribute7Group2;
	private String attribute8Group2;
	private String attribute9Group2;
	private String attribute10Group2;
	private String attribute11Group2;
	private String attribute12Group2;
	private String attribute13Group2;
	private String attribute14Group2;
	private String attribute15Group2;
	private String attribute16Group2;
	private String attribute17Group2;
	private String attribute18Group2;
	private String attribute19Group2;
	private String attribute20Group2;
	private String attribute21Group2;
	private String attribute22Group2;
	private String attribute23Group2;
	private String attribute24Group2;
	private String attribute25Group2;
	private String attribute26Group2;
	private String attribute27Group2;
	private String attribute28Group2;
	private String attribute29Group2;
	private String attribute30Group2;
	private String attribute31Group2;
	private String attribute32Group2;
	private String attribute33Group2;
	private String attribute34Group2;
	private String attribute35Group2;
	private String attribute36Group2;
	private String attribute37Group2;
	private String attribute38Group2;
	private String attribute39Group2;
	private String attribute40Group2;
	private String attribute41Group2;
	private String attribute42Group2;
	private String attribute43Group2;
	private String attribute44Group2;
	private String attribute45Group2;
	private String attribute46Group2;
	private String attribute47Group2;
	private String attribute48Group2;
	private String attribute49Group2;
	private String attribute50Group2;
	private String attribute51Group2;
	private String attribute52Group2;
	private String attribute53Group2;
	private String attribute54Group2;
	private String attribute55Group2;
	private String attribute56Group2;
	private String attribute57Group2;
	private String attribute58Group2;
	private String attribute59Group2;
	private String attribute60Group2;
	private String attribute61Group2;
	private String attribute62Group2;
	private String attribute63Group2;
	private String attribute64Group2;
	private String attribute65Group2;
	private String attribute66Group2;
	private String attribute67Group2;
	private String attribute68Group2;
	private String attribute69Group2;
	private String attribute70Group2;
	private String attribute71Group2;
	private String attribute72Group2;
	private String attribute73Group2;
	private String attribute74Group2;
	private String attribute75Group2;
	private String attribute76Group2;
	private String attribute77Group2;
	private String attribute78Group2;
	private String attribute79Group2;
	private String attribute80Group2;
	private String attribute81Group2;
	private String attribute82Group2;
	private String attribute83Group2;
	private String attribute84Group2;
	private String attribute85Group2;
	private String attribute86Group2;
	private String attribute87Group2;
	private String attribute88Group2;
	private String attribute89Group2;
	private String attribute90Group2;
	private String attribute91Group2;
	private String attribute92Group2;
	private String attribute93Group2;
	private String attribute94Group2;
	private String attribute95Group2;	
	
	/**
	 * @return the attribute1Group1
	 */
	public String getAttribute1Group1() {
		return attribute1Group1;
	}
	/**
	 * @param attribute1Group1 the attribute1Group1 to set
	 */
	public void setAttribute1Group1(String attribute1Group1) {
		this.attribute1Group1 = attribute1Group1;
	}
	/**
	 * @return the attribute2Group1
	 */
	public String getAttribute2Group1() {
		return attribute2Group1;
	}
	/**
	 * @param attribute2Group1 the attribute2Group1 to set
	 */
	public void setAttribute2Group1(String attribute2Group1) {
		this.attribute2Group1 = attribute2Group1;
	}
	/**
	 * @return the attribute1Group2
	 */
	public String getAttribute1Group2() {
		return attribute1Group2;
	}
	/**
	 * @param attribute1Group2 the attribute1Group2 to set
	 */
	public void setAttribute1Group2(String attribute1Group2) {
		this.attribute1Group2 = attribute1Group2;
	}
	/**
	 * @return the attribute2Group2
	 */
	public String getAttribute2Group2() {
		return attribute2Group2;
	}
	/**
	 * @param attribute2Group2 the attribute2Group2 to set
	 */
	public void setAttribute2Group2(String attribute2Group2) {
		this.attribute2Group2 = attribute2Group2;
	}
	/**
	 * @return the attribute3Group2
	 */
	public String getAttribute3Group2() {
		return attribute3Group2;
	}
	/**
	 * @param attribute3Group2 the attribute3Group2 to set
	 */
	public void setAttribute3Group2(String attribute3Group2) {
		this.attribute3Group2 = attribute3Group2;
	}
	/**
	 * @return the attribute4Group2
	 */
	public String getAttribute4Group2() {
		return attribute4Group2;
	}
	/**
	 * @param attribute4Group2 the attribute4Group2 to set
	 */
	public void setAttribute4Group2(String attribute4Group2) {
		this.attribute4Group2 = attribute4Group2;
	}
	/**
	 * @return the attribute5Group2
	 */
	public String getAttribute5Group2() {
		return attribute5Group2;
	}
	/**
	 * @param attribute5Group2 the attribute5Group2 to set
	 */
	public void setAttribute5Group2(String attribute5Group2) {
		this.attribute5Group2 = attribute5Group2;
	}
	/**
	 * @return the attribute6Group2
	 */
	public String getAttribute6Group2() {
		return attribute6Group2;
	}
	/**
	 * @param attribute6Group2 the attribute6Group2 to set
	 */
	public void setAttribute6Group2(String attribute6Group2) {
		this.attribute6Group2 = attribute6Group2;
	}
	/**
	 * @return the attribute7Group2
	 */
	public String getAttribute7Group2() {
		return attribute7Group2;
	}
	/**
	 * @param attribute7Group2 the attribute7Group2 to set
	 */
	public void setAttribute7Group2(String attribute7Group2) {
		this.attribute7Group2 = attribute7Group2;
	}
	/**
	 * @return the attribute8Group2
	 */
	public String getAttribute8Group2() {
		return attribute8Group2;
	}
	/**
	 * @param attribute8Group2 the attribute8Group2 to set
	 */
	public void setAttribute8Group2(String attribute8Group2) {
		this.attribute8Group2 = attribute8Group2;
	}
	/**
	 * @return the attribute9Group2
	 */
	public String getAttribute9Group2() {
		return attribute9Group2;
	}
	/**
	 * @param attribute9Group2 the attribute9Group2 to set
	 */
	public void setAttribute9Group2(String attribute9Group2) {
		this.attribute9Group2 = attribute9Group2;
	}
	/**
	 * @return the attribute10Group2
	 */
	public String getAttribute10Group2() {
		return attribute10Group2;
	}
	/**
	 * @param attribute10Group2 the attribute10Group2 to set
	 */
	public void setAttribute10Group2(String attribute10Group2) {
		this.attribute10Group2 = attribute10Group2;
	}
	/**
	 * @return the attribute11Group2
	 */
	public String getAttribute11Group2() {
		return attribute11Group2;
	}
	/**
	 * @param attribute11Group2 the attribute11Group2 to set
	 */
	public void setAttribute11Group2(String attribute11Group2) {
		this.attribute11Group2 = attribute11Group2;
	}
	/**
	 * @return the attribute12Group2
	 */
	public String getAttribute12Group2() {
		return attribute12Group2;
	}
	/**
	 * @param attribute12Group2 the attribute12Group2 to set
	 */
	public void setAttribute12Group2(String attribute12Group2) {
		this.attribute12Group2 = attribute12Group2;
	}
	/**
	 * @return the attribute13Group2
	 */
	public String getAttribute13Group2() {
		return attribute13Group2;
	}
	/**
	 * @param attribute13Group2 the attribute13Group2 to set
	 */
	public void setAttribute13Group2(String attribute13Group2) {
		this.attribute13Group2 = attribute13Group2;
	}
	/**
	 * @return the attribute14Group2
	 */
	public String getAttribute14Group2() {
		return attribute14Group2;
	}
	/**
	 * @param attribute14Group2 the attribute14Group2 to set
	 */
	public void setAttribute14Group2(String attribute14Group2) {
		this.attribute14Group2 = attribute14Group2;
	}
	/**
	 * @return the attribute15Group2
	 */
	public String getAttribute15Group2() {
		return attribute15Group2;
	}
	/**
	 * @param attribute15Group2 the attribute15Group2 to set
	 */
	public void setAttribute15Group2(String attribute15Group2) {
		this.attribute15Group2 = attribute15Group2;
	}
	/**
	 * @return the attribute16Group2
	 */
	public String getAttribute16Group2() {
		return attribute16Group2;
	}
	/**
	 * @param attribute16Group2 the attribute16Group2 to set
	 */
	public void setAttribute16Group2(String attribute16Group2) {
		this.attribute16Group2 = attribute16Group2;
	}
	/**
	 * @return the attribute17Group2
	 */
	public String getAttribute17Group2() {
		return attribute17Group2;
	}
	/**
	 * @param attribute17Group2 the attribute17Group2 to set
	 */
	public void setAttribute17Group2(String attribute17Group2) {
		this.attribute17Group2 = attribute17Group2;
	}
	/**
	 * @return the attribute18Group2
	 */
	public String getAttribute18Group2() {
		return attribute18Group2;
	}
	/**
	 * @param attribute18Group2 the attribute18Group2 to set
	 */
	public void setAttribute18Group2(String attribute18Group2) {
		this.attribute18Group2 = attribute18Group2;
	}
	/**
	 * @return the attribute19Group2
	 */
	public String getAttribute19Group2() {
		return attribute19Group2;
	}
	/**
	 * @param attribute19Group2 the attribute19Group2 to set
	 */
	public void setAttribute19Group2(String attribute19Group2) {
		this.attribute19Group2 = attribute19Group2;
	}
	/**
	 * @return the attribute20Group2
	 */
	public String getAttribute20Group2() {
		return attribute20Group2;
	}
	/**
	 * @param attribute20Group2 the attribute20Group2 to set
	 */
	public void setAttribute20Group2(String attribute20Group2) {
		this.attribute20Group2 = attribute20Group2;
	}
	/**
	 * @return the attribute21Group2
	 */
	public String getAttribute21Group2() {
		return attribute21Group2;
	}
	/**
	 * @param attribute21Group2 the attribute21Group2 to set
	 */
	public void setAttribute21Group2(String attribute21Group2) {
		this.attribute21Group2 = attribute21Group2;
	}
	/**
	 * @return the attribute22Group2
	 */
	public String getAttribute22Group2() {
		return attribute22Group2;
	}
	/**
	 * @param attribute22Group2 the attribute22Group2 to set
	 */
	public void setAttribute22Group2(String attribute22Group2) {
		this.attribute22Group2 = attribute22Group2;
	}
	/**
	 * @return the attribute23Group2
	 */
	public String getAttribute23Group2() {
		return attribute23Group2;
	}
	/**
	 * @param attribute23Group2 the attribute23Group2 to set
	 */
	public void setAttribute23Group2(String attribute23Group2) {
		this.attribute23Group2 = attribute23Group2;
	}
	/**
	 * @return the attribute24Group2
	 */
	public String getAttribute24Group2() {
		return attribute24Group2;
	}
	/**
	 * @param attribute24Group2 the attribute24Group2 to set
	 */
	public void setAttribute24Group2(String attribute24Group2) {
		this.attribute24Group2 = attribute24Group2;
	}
	/**
	 * @return the attribute25Group2
	 */
	public String getAttribute25Group2() {
		return attribute25Group2;
	}
	/**
	 * @param attribute25Group2 the attribute25Group2 to set
	 */
	public void setAttribute25Group2(String attribute25Group2) {
		this.attribute25Group2 = attribute25Group2;
	}
	/**
	 * @return the attribute26Group2
	 */
	public String getAttribute26Group2() {
		return attribute26Group2;
	}
	/**
	 * @param attribute26Group2 the attribute26Group2 to set
	 */
	public void setAttribute26Group2(String attribute26Group2) {
		this.attribute26Group2 = attribute26Group2;
	}
	/**
	 * @return the attribute27Group2
	 */
	public String getAttribute27Group2() {
		return attribute27Group2;
	}
	/**
	 * @param attribute27Group2 the attribute27Group2 to set
	 */
	public void setAttribute27Group2(String attribute27Group2) {
		this.attribute27Group2 = attribute27Group2;
	}
	/**
	 * @return the attribute28Group2
	 */
	public String getAttribute28Group2() {
		return attribute28Group2;
	}
	/**
	 * @param attribute28Group2 the attribute28Group2 to set
	 */
	public void setAttribute28Group2(String attribute28Group2) {
		this.attribute28Group2 = attribute28Group2;
	}
	/**
	 * @return the attribute3Group1
	 */
	public String getAttribute3Group1() {
		return attribute3Group1;
	}
	/**
	 * @param attribute3Group1 the attribute3Group1 to set
	 */
	public void setAttribute3Group1(String attribute3Group1) {
		this.attribute3Group1 = attribute3Group1;
	}
	/**
	 * @return the attribute4Group1
	 */
	public String getAttribute4Group1() {
		return attribute4Group1;
	}
	/**
	 * @param attribute4Group1 the attribute4Group1 to set
	 */
	public void setAttribute4Group1(String attribute4Group1) {
		this.attribute4Group1 = attribute4Group1;
	}
	/**
	 * @return the attribute5Group1
	 */
	public String getAttribute5Group1() {
		return attribute5Group1;
	}
	/**
	 * @param attribute5Group1 the attribute5Group1 to set
	 */
	public void setAttribute5Group1(String attribute5Group1) {
		this.attribute5Group1 = attribute5Group1;
	}
	/**
	 * @return the attribute29Group2
	 */
	public String getAttribute29Group2() {
		return attribute29Group2;
	}
	/**
	 * @param attribute29Group2 the attribute29Group2 to set
	 */
	public void setAttribute29Group2(String attribute29Group2) {
		this.attribute29Group2 = attribute29Group2;
	}
	/**
	 * @return the attribute30Group2
	 */
	public String getAttribute30Group2() {
		return attribute30Group2;
	}
	/**
	 * @param attribute30Group2 the attribute30Group2 to set
	 */
	public void setAttribute30Group2(String attribute30Group2) {
		this.attribute30Group2 = attribute30Group2;
	}
	/**
	 * @return the attribute31Group2
	 */
	public String getAttribute31Group2() {
		return attribute31Group2;
	}
	/**
	 * @param attribute31Group2 the attribute31Group2 to set
	 */
	public void setAttribute31Group2(String attribute31Group2) {
		this.attribute31Group2 = attribute31Group2;
	}
	/**
	 * @return the attribute32Group2
	 */
	public String getAttribute32Group2() {
		return attribute32Group2;
	}
	/**
	 * @param attribute32Group2 the attribute32Group2 to set
	 */
	public void setAttribute32Group2(String attribute32Group2) {
		this.attribute32Group2 = attribute32Group2;
	}
	/**
	 * @return the attribute33Group2
	 */
	public String getAttribute33Group2() {
		return attribute33Group2;
	}
	/**
	 * @param attribute33Group2 the attribute33Group2 to set
	 */
	public void setAttribute33Group2(String attribute33Group2) {
		this.attribute33Group2 = attribute33Group2;
	}
	/**
	 * @return the attribute34Group2
	 */
	public String getAttribute34Group2() {
		return attribute34Group2;
	}
	/**
	 * @param attribute34Group2 the attribute34Group2 to set
	 */
	public void setAttribute34Group2(String attribute34Group2) {
		this.attribute34Group2 = attribute34Group2;
	}
	/**
	 * @return the attribute35Group2
	 */
	public String getAttribute35Group2() {
		return attribute35Group2;
	}
	/**
	 * @param attribute35Group2 the attribute35Group2 to set
	 */
	public void setAttribute35Group2(String attribute35Group2) {
		this.attribute35Group2 = attribute35Group2;
	}
	/**
	 * @return the attribute36Group2
	 */
	public String getAttribute36Group2() {
		return attribute36Group2;
	}
	/**
	 * @param attribute36Group2 the attribute36Group2 to set
	 */
	public void setAttribute36Group2(String attribute36Group2) {
		this.attribute36Group2 = attribute36Group2;
	}
	/**
	 * @return the attribute37Group2
	 */
	public String getAttribute37Group2() {
		return attribute37Group2;
	}
	/**
	 * @param attribute37Group2 the attribute37Group2 to set
	 */
	public void setAttribute37Group2(String attribute37Group2) {
		this.attribute37Group2 = attribute37Group2;
	}
	/**
	 * @return the attribute38Group2
	 */
	public String getAttribute38Group2() {
		return attribute38Group2;
	}
	/**
	 * @param attribute38Group2 the attribute38Group2 to set
	 */
	public void setAttribute38Group2(String attribute38Group2) {
		this.attribute38Group2 = attribute38Group2;
	}
	/**
	 * @return the attribute39Group2
	 */
	public String getAttribute39Group2() {
		return attribute39Group2;
	}
	/**
	 * @param attribute39Group2 the attribute39Group2 to set
	 */
	public void setAttribute39Group2(String attribute39Group2) {
		this.attribute39Group2 = attribute39Group2;
	}
	/**
	 * @return the attribute40Group2
	 */
	public String getAttribute40Group2() {
		return attribute40Group2;
	}
	/**
	 * @param attribute40Group2 the attribute40Group2 to set
	 */
	public void setAttribute40Group2(String attribute40Group2) {
		this.attribute40Group2 = attribute40Group2;
	}
	/**
	 * @return the attribute41Group2
	 */
	public String getAttribute41Group2() {
		return attribute41Group2;
	}
	/**
	 * @param attribute41Group2 the attribute41Group2 to set
	 */
	public void setAttribute41Group2(String attribute41Group2) {
		this.attribute41Group2 = attribute41Group2;
	}
	/**
	 * @return the attribute42Group2
	 */
	public String getAttribute42Group2() {
		return attribute42Group2;
	}
	/**
	 * @param attribute42Group2 the attribute42Group2 to set
	 */
	public void setAttribute42Group2(String attribute42Group2) {
		this.attribute42Group2 = attribute42Group2;
	}
	/**
	 * @return the attribute43Group2
	 */
	public String getAttribute43Group2() {
		return attribute43Group2;
	}
	/**
	 * @param attribute43Group2 the attribute43Group2 to set
	 */
	public void setAttribute43Group2(String attribute43Group2) {
		this.attribute43Group2 = attribute43Group2;
	}
	/**
	 * @return the attribute44Group2
	 */
	public String getAttribute44Group2() {
		return attribute44Group2;
	}
	/**
	 * @param attribute44Group2 the attribute44Group2 to set
	 */
	public void setAttribute44Group2(String attribute44Group2) {
		this.attribute44Group2 = attribute44Group2;
	}
	/**
	 * @return the attribute45Group2
	 */
	public String getAttribute45Group2() {
		return attribute45Group2;
	}
	/**
	 * @param attribute45Group2 the attribute45Group2 to set
	 */
	public void setAttribute45Group2(String attribute45Group2) {
		this.attribute45Group2 = attribute45Group2;
	}
	/**
	 * @return the attribute46Group2
	 */
	public String getAttribute46Group2() {
		return attribute46Group2;
	}
	/**
	 * @param attribute46Group2 the attribute46Group2 to set
	 */
	public void setAttribute46Group2(String attribute46Group2) {
		this.attribute46Group2 = attribute46Group2;
	}
	/**
	 * @return the attribute47Group2
	 */
	public String getAttribute47Group2() {
		return attribute47Group2;
	}
	/**
	 * @param attribute47Group2 the attribute47Group2 to set
	 */
	public void setAttribute47Group2(String attribute47Group2) {
		this.attribute47Group2 = attribute47Group2;
	}
	/**
	 * @return the attribute48Group2
	 */
	public String getAttribute48Group2() {
		return attribute48Group2;
	}
	/**
	 * @param attribute48Group2 the attribute48Group2 to set
	 */
	public void setAttribute48Group2(String attribute48Group2) {
		this.attribute48Group2 = attribute48Group2;
	}
	/**
	 * @return the attribute49Group2
	 */
	public String getAttribute49Group2() {
		return attribute49Group2;
	}
	/**
	 * @param attribute49Group2 the attribute49Group2 to set
	 */
	public void setAttribute49Group2(String attribute49Group2) {
		this.attribute49Group2 = attribute49Group2;
	}
	/**
	 * @return the attribute50Group2
	 */
	public String getAttribute50Group2() {
		return attribute50Group2;
	}
	/**
	 * @param attribute50Group2 the attribute50Group2 to set
	 */
	public void setAttribute50Group2(String attribute50Group2) {
		this.attribute50Group2 = attribute50Group2;
	}
	/**
	 * @return the attribute51Group2
	 */
	public String getAttribute51Group2() {
		return attribute51Group2;
	}
	/**
	 * @param attribute51Group2 the attribute51Group2 to set
	 */
	public void setAttribute51Group2(String attribute51Group2) {
		this.attribute51Group2 = attribute51Group2;
	}
	/**
	 * @return the attribute52Group2
	 */
	public String getAttribute52Group2() {
		return attribute52Group2;
	}
	/**
	 * @param attribute52Group2 the attribute52Group2 to set
	 */
	public void setAttribute52Group2(String attribute52Group2) {
		this.attribute52Group2 = attribute52Group2;
	}
	/**
	 * @return the attribute53Group2
	 */
	public String getAttribute53Group2() {
		return attribute53Group2;
	}
	/**
	 * @param attribute53Group2 the attribute53Group2 to set
	 */
	public void setAttribute53Group2(String attribute53Group2) {
		this.attribute53Group2 = attribute53Group2;
	}
	/**
	 * @return the attribute54Group2
	 */
	public String getAttribute54Group2() {
		return attribute54Group2;
	}
	/**
	 * @param attribute54Group2 the attribute54Group2 to set
	 */
	public void setAttribute54Group2(String attribute54Group2) {
		this.attribute54Group2 = attribute54Group2;
	}
	/**
	 * @return the attribute55Group2
	 */
	public String getAttribute55Group2() {
		return attribute55Group2;
	}
	/**
	 * @param attribute55Group2 the attribute55Group2 to set
	 */
	public void setAttribute55Group2(String attribute55Group2) {
		this.attribute55Group2 = attribute55Group2;
	}
	/**
	 * @return the attribute56Group2
	 */
	public String getAttribute56Group2() {
		return attribute56Group2;
	}
	/**
	 * @param attribute56Group2 the attribute56Group2 to set
	 */
	public void setAttribute56Group2(String attribute56Group2) {
		this.attribute56Group2 = attribute56Group2;
	}
	/**
	 * @return the attribute57Group2
	 */
	public String getAttribute57Group2() {
		return attribute57Group2;
	}
	/**
	 * @param attribute57Group2 the attribute57Group2 to set
	 */
	public void setAttribute57Group2(String attribute57Group2) {
		this.attribute57Group2 = attribute57Group2;
	}
	/**
	 * @return the attribute58Group2
	 */
	public String getAttribute58Group2() {
		return attribute58Group2;
	}
	/**
	 * @param attribute58Group2 the attribute58Group2 to set
	 */
	public void setAttribute58Group2(String attribute58Group2) {
		this.attribute58Group2 = attribute58Group2;
	}
	/**
	 * @return the attribute59Group2
	 */
	public String getAttribute59Group2() {
		return attribute59Group2;
	}
	/**
	 * @param attribute59Group2 the attribute59Group2 to set
	 */
	public void setAttribute59Group2(String attribute59Group2) {
		this.attribute59Group2 = attribute59Group2;
	}
	/**
	 * @return the attribute60Group2
	 */
	public String getAttribute60Group2() {
		return attribute60Group2;
	}
	/**
	 * @param attribute60Group2 the attribute60Group2 to set
	 */
	public void setAttribute60Group2(String attribute60Group2) {
		this.attribute60Group2 = attribute60Group2;
	}
	/**
	 * @return the attribute61Group2
	 */
	public String getAttribute61Group2() {
		return attribute61Group2;
	}
	/**
	 * @param attribute61Group2 the attribute61Group2 to set
	 */
	public void setAttribute61Group2(String attribute61Group2) {
		this.attribute61Group2 = attribute61Group2;
	}
	/**
	 * @return the attribute62Group2
	 */
	public String getAttribute62Group2() {
		return attribute62Group2;
	}
	/**
	 * @param attribute62Group2 the attribute62Group2 to set
	 */
	public void setAttribute62Group2(String attribute62Group2) {
		this.attribute62Group2 = attribute62Group2;
	}
	/**
	 * @return the attribute63Group2
	 */
	public String getAttribute63Group2() {
		return attribute63Group2;
	}
	/**
	 * @param attribute63Group2 the attribute63Group2 to set
	 */
	public void setAttribute63Group2(String attribute63Group2) {
		this.attribute63Group2 = attribute63Group2;
	}
	/**
	 * @return the attribute64Group2
	 */
	public String getAttribute64Group2() {
		return attribute64Group2;
	}
	/**
	 * @param attribute64Group2 the attribute64Group2 to set
	 */
	public void setAttribute64Group2(String attribute64Group2) {
		this.attribute64Group2 = attribute64Group2;
	}
	/**
	 * @return the attribute65Group2
	 */
	public String getAttribute65Group2() {
		return attribute65Group2;
	}
	/**
	 * @param attribute65Group2 the attribute65Group2 to set
	 */
	public void setAttribute65Group2(String attribute65Group2) {
		this.attribute65Group2 = attribute65Group2;
	}
	/**
	 * @return the attribute66Group2
	 */
	public String getAttribute66Group2() {
		return attribute66Group2;
	}
	/**
	 * @param attribute66Group2 the attribute66Group2 to set
	 */
	public void setAttribute66Group2(String attribute66Group2) {
		this.attribute66Group2 = attribute66Group2;
	}
	/**
	 * @return the attribute67Group2
	 */
	public String getAttribute67Group2() {
		return attribute67Group2;
	}
	/**
	 * @param attribute67Group2 the attribute67Group2 to set
	 */
	public void setAttribute67Group2(String attribute67Group2) {
		this.attribute67Group2 = attribute67Group2;
	}
	/**
	 * @return the attribute68Group2
	 */
	public String getAttribute68Group2() {
		return attribute68Group2;
	}
	/**
	 * @param attribute68Group2 the attribute68Group2 to set
	 */
	public void setAttribute68Group2(String attribute68Group2) {
		this.attribute68Group2 = attribute68Group2;
	}
	/**
	 * @return the attribute69Group2
	 */
	public String getAttribute69Group2() {
		return attribute69Group2;
	}
	/**
	 * @param attribute69Group2 the attribute69Group2 to set
	 */
	public void setAttribute69Group2(String attribute69Group2) {
		this.attribute69Group2 = attribute69Group2;
	}
	/**
	 * @return the attribute70Group2
	 */
	public String getAttribute70Group2() {
		return attribute70Group2;
	}
	/**
	 * @param attribute70Group2 the attribute70Group2 to set
	 */
	public void setAttribute70Group2(String attribute70Group2) {
		this.attribute70Group2 = attribute70Group2;
	}
	/**
	 * @return the attribute71Group2
	 */
	public String getAttribute71Group2() {
		return attribute71Group2;
	}
	/**
	 * @param attribute71Group2 the attribute71Group2 to set
	 */
	public void setAttribute71Group2(String attribute71Group2) {
		this.attribute71Group2 = attribute71Group2;
	}
	/**
	 * @return the attribute72Group2
	 */
	public String getAttribute72Group2() {
		return attribute72Group2;
	}
	/**
	 * @param attribute72Group2 the attribute72Group2 to set
	 */
	public void setAttribute72Group2(String attribute72Group2) {
		this.attribute72Group2 = attribute72Group2;
	}
	/**
	 * @return the attribute73Group2
	 */
	public String getAttribute73Group2() {
		return attribute73Group2;
	}
	/**
	 * @param attribute73Group2 the attribute73Group2 to set
	 */
	public void setAttribute73Group2(String attribute73Group2) {
		this.attribute73Group2 = attribute73Group2;
	}
	/**
	 * @return the attribute74Group2
	 */
	public String getAttribute74Group2() {
		return attribute74Group2;
	}
	/**
	 * @param attribute74Group2 the attribute74Group2 to set
	 */
	public void setAttribute74Group2(String attribute74Group2) {
		this.attribute74Group2 = attribute74Group2;
	}
	/**
	 * @return the attribute75Group2
	 */
	public String getAttribute75Group2() {
		return attribute75Group2;
	}
	/**
	 * @param attribute75Group2 the attribute75Group2 to set
	 */
	public void setAttribute75Group2(String attribute75Group2) {
		this.attribute75Group2 = attribute75Group2;
	}
	/**
	 * @return the attribute76Group2
	 */
	public String getAttribute76Group2() {
		return attribute76Group2;
	}
	/**
	 * @param attribute76Group2 the attribute76Group2 to set
	 */
	public void setAttribute76Group2(String attribute76Group2) {
		this.attribute76Group2 = attribute76Group2;
	}
	/**
	 * @return the attribute77Group2
	 */
	public String getAttribute77Group2() {
		return attribute77Group2;
	}
	/**
	 * @param attribute77Group2 the attribute77Group2 to set
	 */
	public void setAttribute77Group2(String attribute77Group2) {
		this.attribute77Group2 = attribute77Group2;
	}
	/**
	 * @return the attribute78Group2
	 */
	public String getAttribute78Group2() {
		return attribute78Group2;
	}
	/**
	 * @param attribute78Group2 the attribute78Group2 to set
	 */
	public void setAttribute78Group2(String attribute78Group2) {
		this.attribute78Group2 = attribute78Group2;
	}
	/**
	 * @return the attribute79Group2
	 */
	public String getAttribute79Group2() {
		return attribute79Group2;
	}
	/**
	 * @param attribute79Group2 the attribute79Group2 to set
	 */
	public void setAttribute79Group2(String attribute79Group2) {
		this.attribute79Group2 = attribute79Group2;
	}
	/**
	 * @return the attribute80Group2
	 */
	public String getAttribute80Group2() {
		return attribute80Group2;
	}
	/**
	 * @param attribute80Group2 the attribute80Group2 to set
	 */
	public void setAttribute80Group2(String attribute80Group2) {
		this.attribute80Group2 = attribute80Group2;
	}
	/**
	 * @return the attribute81Group2
	 */
	public String getAttribute81Group2() {
		return attribute81Group2;
	}
	/**
	 * @param attribute81Group2 the attribute81Group2 to set
	 */
	public void setAttribute81Group2(String attribute81Group2) {
		this.attribute81Group2 = attribute81Group2;
	}
	/**
	 * @return the attribute82Group2
	 */
	public String getAttribute82Group2() {
		return attribute82Group2;
	}
	/**
	 * @param attribute82Group2 the attribute82Group2 to set
	 */
	public void setAttribute82Group2(String attribute82Group2) {
		this.attribute82Group2 = attribute82Group2;
	}
	/**
	 * @return the attribute83Group2
	 */
	public String getAttribute83Group2() {
		return attribute83Group2;
	}
	/**
	 * @param attribute83Group2 the attribute83Group2 to set
	 */
	public void setAttribute83Group2(String attribute83Group2) {
		this.attribute83Group2 = attribute83Group2;
	}
	/**
	 * @return the attribute84Group2
	 */
	public String getAttribute84Group2() {
		return attribute84Group2;
	}
	/**
	 * @param attribute84Group2 the attribute84Group2 to set
	 */
	public void setAttribute84Group2(String attribute84Group2) {
		this.attribute84Group2 = attribute84Group2;
	}
	/**
	 * @return the attribute85Group2
	 */
	public String getAttribute85Group2() {
		return attribute85Group2;
	}
	/**
	 * @param attribute85Group2 the attribute85Group2 to set
	 */
	public void setAttribute85Group2(String attribute85Group2) {
		this.attribute85Group2 = attribute85Group2;
	}
	/**
	 * @return the attribute86Group2
	 */
	public String getAttribute86Group2() {
		return attribute86Group2;
	}
	/**
	 * @param attribute86Group2 the attribute86Group2 to set
	 */
	public void setAttribute86Group2(String attribute86Group2) {
		this.attribute86Group2 = attribute86Group2;
	}
	/**
	 * @return the attribute87Group2
	 */
	public String getAttribute87Group2() {
		return attribute87Group2;
	}
	/**
	 * @param attribute87Group2 the attribute87Group2 to set
	 */
	public void setAttribute87Group2(String attribute87Group2) {
		this.attribute87Group2 = attribute87Group2;
	}
	/**
	 * @return the attribute88Group2
	 */
	public String getAttribute88Group2() {
		return attribute88Group2;
	}
	/**
	 * @param attribute88Group2 the attribute88Group2 to set
	 */
	public void setAttribute88Group2(String attribute88Group2) {
		this.attribute88Group2 = attribute88Group2;
	}
	/**
	 * @return the attribute89Group2
	 */
	public String getAttribute89Group2() {
		return attribute89Group2;
	}
	/**
	 * @param attribute89Group2 the attribute89Group2 to set
	 */
	public void setAttribute89Group2(String attribute89Group2) {
		this.attribute89Group2 = attribute89Group2;
	}
	/**
	 * @return the attribute90Group2
	 */
	public String getAttribute90Group2() {
		return attribute90Group2;
	}
	/**
	 * @param attribute90Group2 the attribute90Group2 to set
	 */
	public void setAttribute90Group2(String attribute90Group2) {
		this.attribute90Group2 = attribute90Group2;
	}
	/**
	 * @return the attribute91Group2
	 */
	public String getAttribute91Group2() {
		return attribute91Group2;
	}
	/**
	 * @param attribute91Group2 the attribute91Group2 to set
	 */
	public void setAttribute91Group2(String attribute91Group2) {
		this.attribute91Group2 = attribute91Group2;
	}
	/**
	 * @return the attribute92Group2
	 */
	public String getAttribute92Group2() {
		return attribute92Group2;
	}
	/**
	 * @param attribute92Group2 the attribute92Group2 to set
	 */
	public void setAttribute92Group2(String attribute92Group2) {
		this.attribute92Group2 = attribute92Group2;
	}
	/**
	 * @return the attribute93Group2
	 */
	public String getAttribute93Group2() {
		return attribute93Group2;
	}
	/**
	 * @param attribute93Group2 the attribute93Group2 to set
	 */
	public void setAttribute93Group2(String attribute93Group2) {
		this.attribute93Group2 = attribute93Group2;
	}
	/**
	 * @return the attribute94Group2
	 */
	public String getAttribute94Group2() {
		return attribute94Group2;
	}
	/**
	 * @param attribute94Group2 the attribute94Group2 to set
	 */
	public void setAttribute94Group2(String attribute94Group2) {
		this.attribute94Group2 = attribute94Group2;
	}
	/**
	 * @return the attribute95Group2
	 */
	public String getAttribute95Group2() {
		return attribute95Group2;
	}
	/**
	 * @param attribute95Group2 the attribute95Group2 to set
	 */
	public void setAttribute95Group2(String attribute95Group2) {
		this.attribute95Group2 = attribute95Group2;
	}
}
