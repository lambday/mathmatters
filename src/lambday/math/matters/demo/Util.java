/**
 * 
 */
package lambday.math.matters.demo;

/**
 * @author lambday
 *
 */
public class Util {
	/**
	 * @param string
	 * @return
	 */
	public static boolean isNull(String string) {
		return string == null;
	}

	/**
	 * @param skewedPOJO
	 * @return
	 */
	public static boolean isAllOfGroup1Absent(SkewedPOJO skewedPOJO) {
		return !Util.isAnyOfGroup1Present(skewedPOJO);
	}

	/**
	 * @param skewedPOJO
	 * @return
	 */
	public static boolean isAllOfGroup2Absent(SkewedPOJO skewedPOJO) {
		return !Util.isAnyOfGroup2Present(skewedPOJO);
	}

	/**
	 * @param skewedPOJO
	 * @return
	 */
	public static boolean isAnyOfGroup1Present(SkewedPOJO skewedPOJO) {
		return	!isNull(skewedPOJO.getAttribute1Group1()) ||
				!isNull(skewedPOJO.getAttribute2Group1()) ||
				!isNull(skewedPOJO.getAttribute3Group1()) ||
				!isNull(skewedPOJO.getAttribute4Group1()) ||
				!isNull(skewedPOJO.getAttribute5Group1());
	}

	/**
	 * @param skewedPOJO
	 * @return
	 */
	public static boolean isAnyOfGroup2Present(SkewedPOJO skewedPOJO) {
		return	!isNull(skewedPOJO.getAttribute1Group2()) ||
				!isNull(skewedPOJO.getAttribute2Group2()) ||
				!isNull(skewedPOJO.getAttribute3Group2()) ||
				!isNull(skewedPOJO.getAttribute4Group2()) ||
				!isNull(skewedPOJO.getAttribute5Group2()) ||
				!isNull(skewedPOJO.getAttribute6Group2()) ||
				!isNull(skewedPOJO.getAttribute7Group2()) ||
				!isNull(skewedPOJO.getAttribute8Group2()) ||
				!isNull(skewedPOJO.getAttribute9Group2()) ||
				!isNull(skewedPOJO.getAttribute10Group2()) ||
				!isNull(skewedPOJO.getAttribute11Group2()) ||
				!isNull(skewedPOJO.getAttribute12Group2()) ||
				!isNull(skewedPOJO.getAttribute13Group2()) ||
				!isNull(skewedPOJO.getAttribute14Group2()) ||
				!isNull(skewedPOJO.getAttribute15Group2()) ||
				!isNull(skewedPOJO.getAttribute16Group2()) ||
				!isNull(skewedPOJO.getAttribute17Group2()) ||
				!isNull(skewedPOJO.getAttribute18Group2()) ||
				!isNull(skewedPOJO.getAttribute19Group2()) ||
				!isNull(skewedPOJO.getAttribute20Group2()) ||
				!isNull(skewedPOJO.getAttribute21Group2()) ||
				!isNull(skewedPOJO.getAttribute22Group2()) ||
				!isNull(skewedPOJO.getAttribute23Group2()) ||
				!isNull(skewedPOJO.getAttribute24Group2()) ||
				!isNull(skewedPOJO.getAttribute25Group2()) ||
				!isNull(skewedPOJO.getAttribute26Group2()) ||
				!isNull(skewedPOJO.getAttribute27Group2()) ||
				!isNull(skewedPOJO.getAttribute28Group2()) ||
				!isNull(skewedPOJO.getAttribute29Group2()) ||
				!isNull(skewedPOJO.getAttribute30Group2()) ||
				!isNull(skewedPOJO.getAttribute31Group2()) ||
				!isNull(skewedPOJO.getAttribute32Group2()) ||
				!isNull(skewedPOJO.getAttribute33Group2()) ||
				!isNull(skewedPOJO.getAttribute34Group2()) ||
				!isNull(skewedPOJO.getAttribute35Group2()) ||
				!isNull(skewedPOJO.getAttribute36Group2()) ||
				!isNull(skewedPOJO.getAttribute37Group2()) ||
				!isNull(skewedPOJO.getAttribute38Group2()) ||
				!isNull(skewedPOJO.getAttribute39Group2()) ||
				!isNull(skewedPOJO.getAttribute40Group2()) ||
				!isNull(skewedPOJO.getAttribute41Group2()) ||
				!isNull(skewedPOJO.getAttribute42Group2()) ||
				!isNull(skewedPOJO.getAttribute43Group2()) ||
				!isNull(skewedPOJO.getAttribute44Group2()) ||
				!isNull(skewedPOJO.getAttribute45Group2()) ||
				!isNull(skewedPOJO.getAttribute46Group2()) ||
				!isNull(skewedPOJO.getAttribute47Group2()) ||
				!isNull(skewedPOJO.getAttribute48Group2()) ||
				!isNull(skewedPOJO.getAttribute49Group2()) ||
				!isNull(skewedPOJO.getAttribute50Group2()) ||
				!isNull(skewedPOJO.getAttribute51Group2()) ||
				!isNull(skewedPOJO.getAttribute52Group2()) ||
				!isNull(skewedPOJO.getAttribute53Group2()) ||
				!isNull(skewedPOJO.getAttribute54Group2()) ||
				!isNull(skewedPOJO.getAttribute55Group2()) ||
				!isNull(skewedPOJO.getAttribute56Group2()) ||
				!isNull(skewedPOJO.getAttribute57Group2()) ||
				!isNull(skewedPOJO.getAttribute58Group2()) ||
				!isNull(skewedPOJO.getAttribute59Group2()) ||
				!isNull(skewedPOJO.getAttribute60Group2()) ||
				!isNull(skewedPOJO.getAttribute61Group2()) ||
				!isNull(skewedPOJO.getAttribute62Group2()) ||
				!isNull(skewedPOJO.getAttribute63Group2()) ||
				!isNull(skewedPOJO.getAttribute64Group2()) ||
				!isNull(skewedPOJO.getAttribute65Group2()) ||
				!isNull(skewedPOJO.getAttribute66Group2()) ||
				!isNull(skewedPOJO.getAttribute67Group2()) ||
				!isNull(skewedPOJO.getAttribute68Group2()) ||
				!isNull(skewedPOJO.getAttribute69Group2()) ||
				!isNull(skewedPOJO.getAttribute70Group2()) ||
				!isNull(skewedPOJO.getAttribute71Group2()) ||
				!isNull(skewedPOJO.getAttribute72Group2()) ||
				!isNull(skewedPOJO.getAttribute73Group2()) ||
				!isNull(skewedPOJO.getAttribute74Group2()) ||
				!isNull(skewedPOJO.getAttribute75Group2()) ||
				!isNull(skewedPOJO.getAttribute76Group2()) ||
				!isNull(skewedPOJO.getAttribute77Group2()) ||
				!isNull(skewedPOJO.getAttribute78Group2()) ||
				!isNull(skewedPOJO.getAttribute79Group2()) ||
				!isNull(skewedPOJO.getAttribute80Group2()) ||
				!isNull(skewedPOJO.getAttribute81Group2()) ||
				!isNull(skewedPOJO.getAttribute82Group2()) ||
				!isNull(skewedPOJO.getAttribute83Group2()) ||
				!isNull(skewedPOJO.getAttribute84Group2()) ||
				!isNull(skewedPOJO.getAttribute85Group2()) ||
				!isNull(skewedPOJO.getAttribute86Group2()) ||
				!isNull(skewedPOJO.getAttribute87Group2()) ||
				!isNull(skewedPOJO.getAttribute88Group2()) ||
				!isNull(skewedPOJO.getAttribute89Group2()) ||
				!isNull(skewedPOJO.getAttribute90Group2()) ||
				!isNull(skewedPOJO.getAttribute91Group2()) ||
				!isNull(skewedPOJO.getAttribute92Group2()) ||
				!isNull(skewedPOJO.getAttribute93Group2()) ||
				!isNull(skewedPOJO.getAttribute94Group2()) ||
				!isNull(skewedPOJO.getAttribute95Group2());
	}
}
